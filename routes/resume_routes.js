var express = require('express');
var router = express.Router();
var resume_dal = require('../model/resume_dal');
var account_dal = require('../model/account_dal');


// View All resumes
router.get('/all', function(req, res) {
    resume_dal.getAll(function(err, result){
        if(err) {
            res.send(err);
        }
        else {
            res.render('resume/resumeViewAll', { 'result':result });
        }
    });

});

// Return the user to add form
router.get('/add/selectuser', function(req, res){
    // passing all the query parameters (req.query) to the insert function instead of each individually
    account_dal.getAll(function(err,result) {
        if (err) {
            res.send(err);
        }
        else {
            res.render('resume/resumeAddUser', {'account': result});
        }
    });
});

// Returns resume to resume all
router.get('/add', function(req, res) {
    resume_dal.getAll(function(err,result) {
            if (err) {
                res.send(err);
            }
            else {
                res.render('resume/resumeAdd', {'resume' : result});
            }
    });
});

router.get('/insert', function(req, res){
    // simple validation
    console.log(req.query);
    if(!(req.query.account_id)) {
        res.send('All fields must be provided');
    }
    else {
        // passing all the query parameters (req.query) to the insert function instead of each individually
        resume_dal.getData(req.query.account_id, function(err,result) {
            if (err) {
                console.log(err);
                res.send(err);
            }
            else {
                res.render('resume/resumeAdd', {account : req.query.account_id, schools : result[0], company : result[1], skills : result[2]});
            }
        });
    }
});

router.get('/insert/resume', function(req, res) {
    // validation
    //passing the query parameters
    console.log(req.query);
    resume_dal.insert(req.query, function(err, result) {
        if (err) {
            console.log(err);
        }
        else {
            res.send("success");
        }
    });
});


module.exports = router;
