/**
 * Created by Mbecnel on 4/18/2017.
 */
var mysql   = require('mysql');
var db  = require('./db_connection.js');

/* DATABASE CONFIGURATION */
var connection = mysql.createConnection(db.config);


exports.getAll = function(callback) {
    var query = 'Select a.first_name, a.last_name, r.resume_name From account a Left Join resume r on resume_id = a.account_id;';

    connection.query(query, function(err, result) {
        callback(err, result);
    });
};

exports.getData = function(account_id, callback) {
    var query = 'CALL account_data(?)';
    var queryData = [account_id];
    console.log(query);

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};

exports.insert = function(params, callback) {

    // FIRST INSERT THE COMPANY
    var query = 'insert into resume (account_id, resume_name) values (?, ?)';

    var queryData = [params.account_id, params.resume_name];

    var resumeID;

    connection.query(query, queryData, function(err, result) {

        // THEN USE THE COMPANY_ID RETURNED AS insertId AND THE SELECTED ADDRESS_IDs INTO COMPANY_ADDRESS
        resumeID = result.insertId;

        // NOTE THAT THERE IS ONLY ONE QUESTION MARK IN VALUES ?
        query = 'insert into resume_school (resume_id, school_id) values (?, ?)';

        // TO BULK INSERT RECORDS WE CREATE A MULTIDIMENSIONAL ARRAY OF THE VALUES
        queryData = [resumeID, params.school_id];

        connection.query(query, queryData, function(err, result) {

            query = 'insert into resume_company (resume_id, company_id) values (?, ?)';

            queryData = [resumeID, params.company_id];

            connection.query(query, queryData, function(err, result) {

                query = 'insert into resume_skill (skill_id, resume_id) values (?, ?)';

                queryData = [params.skill_id, resumeID];

                connection.query(query, queryData, function (err, result)  {
                    callback(err, result);

                });
            });
        });
    });

};



